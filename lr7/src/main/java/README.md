Classes, avilable in this package:
 * Constants - set of constants
 * DrawableDijkstra2 - class, which contains realization of dijkstra algorithm. 
 Realization written by the help of: https://www.codeflow.site/ru/article/java-dijkstra
 * Data classes:
    * DrawableDot - class, which represents a dot
    * DrawableGraph - class, which represents a graph
    * DrawableLine - class, which represents a line between dots
 * DrawableXMLParser - contains methods to parse test1.xml file