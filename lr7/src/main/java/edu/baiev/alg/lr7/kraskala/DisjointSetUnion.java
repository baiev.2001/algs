package edu.baiev.alg.lr7.kraskala;

public interface DisjointSetUnion<I> {
    void makeSet(I line);

    int find(I line);

    boolean unite(I item1, I item2);
}
