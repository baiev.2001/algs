package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.line.DrawableLine;
import edu.baiev.alg.lr7.line.LineService;

import java.awt.*;
import java.util.List;
import java.util.*;

public class DrawableDijkstra2 {
    private final LineService lineService;
    private static final List<Color> availableVectorColors = Arrays.asList(
            Color.ORANGE,
            Color.RED,
            Color.GREEN,
            Color.CYAN,
            Color.MAGENTA,
            new Color(104, 25, 215),
            new Color(177, 208, 19, 255)
    );

    public DrawableDijkstra2() {
        this.lineService = new LineService();
    }

    public DrawableGraph calculateShortestPathFromSourceAndPaint(DrawableGraph graph, DrawableDot source) {
        Iterator<Color> iterator = availableVectorColors.iterator();
        source.setDistance(0);
        Set<DrawableDot> settledDots = new HashSet<>();
        Set<DrawableDot> unsettledDots = new HashSet<>();
        unsettledDots.add(source);
        while (unsettledDots.size() != 0) {
            DrawableDot currentDot = getLowestDistanceDot(unsettledDots);
            currentDot.setColor(Color.YELLOW);
            graph.repaint();
            waitForAnimation();
            unsettledDots.remove(currentDot);
            if (!iterator.hasNext()) {
                iterator = availableVectorColors.iterator();
            }
            Color lineColor = iterator.next();
            for (DrawableLine line : lineService.getLinesByStartDot(currentDot, graph.getDrawableLines())) {
                if (!settledDots.contains(line.getDrawableDotTo())) {
                    line.getDrawableDotTo().setColor(Color.BLUE);
                    line.setColor(lineColor);
                    graph.repaint();
                    waitForAnimation();
                    calculateMinimumDistance(currentDot, line);
                    unsettledDots.add(line.getDrawableDotTo());
                }
            }
            settledDots.add(currentDot);
            currentDot.setColor(Color.BLUE);
            graph.repaint();
            waitForAnimation();
        }
        graph.repaint();
        return graph;
    }

    private void calculateMinimumDistance(DrawableDot sourceDot, DrawableLine line) {
        DrawableDot evaluationDot = line.getDrawableDotTo();
        int sourceDistance = sourceDot.getDistance();
        if (sourceDistance + line.getWeight() < evaluationDot.getDistance()) {
            evaluationDot.setDistance(sourceDistance + line.getWeight());
            LinkedList<DrawableLine> shortestPath = new LinkedList<>(sourceDot.getShortestPath());
            shortestPath.add(line);
            evaluationDot.setShortestPath(shortestPath);
        }
    }

    private DrawableDot getLowestDistanceDot(Set<DrawableDot> unsettledDots) {
        int minWeight = Integer.MAX_VALUE;
        DrawableDot lowestDot = null;
        for (DrawableDot dot : unsettledDots) {
            int dotWeight = dot.getDistance();
            if (dotWeight < minWeight) {
                minWeight = dotWeight;
                lowestDot = dot;
            }
        }
        return lowestDot;
    }


    private void waitForAnimation() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}

