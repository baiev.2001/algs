package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.line.DrawableLine;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class DrawableGraph extends JComponent {
    private int id;
    private Set<DrawableDot> drawableDots = new HashSet<>();
    private Set<DrawableLine> drawableLines = new HashSet<>();

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.black);
        g.drawString("Graph id: " + id, 50, 50);
        drawableDots.forEach(dot -> dot.paintComponent(g));
        drawableLines.forEach(dot -> dot.paintComponent(g));
    }

    @Override
    public Dimension getPreferredSize() {
        int xOffset = this.drawableDots.stream()
                .mapToInt(DrawableDot::getX)
                .min()
                .orElseGet(() -> 0);
        int yOffset = this.drawableDots.stream()
                .mapToInt(DrawableDot::getY)
                .min()
                .orElseGet(() -> 0);
        int x = this.drawableDots.stream()
                .mapToInt(DrawableDot::getX)
                .max()
                .orElseGet(() -> 100);
        int y = this.drawableDots.stream()
                .mapToInt(DrawableDot::getY)
                .max()
                .orElseGet(() -> 100);
        return new Dimension(x + xOffset, y + yOffset);
    }

    public void addDot(DrawableDot drawableDot) {
        this.drawableDots.add(drawableDot);
    }

    public void addLine(DrawableLine dot) {
        this.drawableLines.add(dot);
    }
}
