package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.line.DrawableLine;
import edu.baiev.alg.lr7.line.LineService;

import java.awt.*;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DrawablePrima2 {
    private final LineService lineService;


    public DrawablePrima2() {
        this.lineService = new LineService();
    }


    public void findMinimalTree(DrawableGraph graph) {
        Set<DrawableDot> drawableDots = graph.getDrawableDots();
        DrawableDot sourceDot = drawableDots.stream().findFirst().get();
        sourceDot.setDistance(0);

        Set<DrawableDot> settledDots = new HashSet<>();
        settledDots.add(sourceDot);
        Set<DrawableLine> algLines = new HashSet<>(graph.getDrawableLines());
        while (settledDots.size() != graph.getDrawableDots().size()) {
            Map.Entry<DrawableLine, DrawableDot> lowestCommunicationgEdge = findLowestCommunicationgEdge(settledDots, algLines);
            lowestCommunicationgEdge.getKey().setColor(Color.RED);
            graph.repaint();
            waitForAnimation();
            settledDots.add(lowestCommunicationgEdge.getValue());
            filterCyclumEdge(algLines, settledDots);
        }
    }

    private void filterCyclumEdge(Set<DrawableLine> lines, Set<DrawableDot> settledDots) {
        lines.removeIf(line -> (settledDots.contains(line.getDrawableDotFrom()) && settledDots.contains(line.getDrawableDotTo())));
    }

    private Map.Entry<DrawableLine, DrawableDot> findLowestCommunicationgEdge(Set<DrawableDot> settledDots, Set<DrawableLine> sourceLines) {
        Map<DrawableLine, DrawableDot> lowestEdgesMap = settledDots.stream()
                .filter(settledDot -> sourceLines.stream()
                        .flatMap(line -> Stream.of(line.getDrawableDotTo(), line.getDrawableDotFrom()))
                        .anyMatch(dot -> dot.equals(settledDot)))
                .collect(Collectors.toMap(
                        (DrawableDot dot) -> lineService.getLightestEdgeByDot(dot, sourceLines),
                        (DrawableDot dot) -> lineService.getOppositeDot(lineService.getLightestEdgeByDot(dot, sourceLines), dot)
                ));

        return lowestEdgesMap
                .entrySet()
                .stream()
                .min(Comparator.comparingInt((Map.Entry<DrawableLine, DrawableDot> entry) -> entry.getKey().getWeight()))
                .orElseThrow(() -> new RuntimeException("dddd"));
    }

    private void waitForAnimation() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

