package edu.baiev.alg.lr7;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static edu.baiev.alg.lr7.Constants.PATH;

public class FrameApp2 extends JFrame {
    private final DrawableXMLParser xmlParser = new DrawableXMLParser();
    private final DrawableDijkstra2 dijkstra2 = new DrawableDijkstra2();
    private final DrawablePrima2 prima = new DrawablePrima2();
    private final DrawableKraskala kraskala = new DrawableKraskala();
    private final DrawableFlow flowSearch = new DrawableFlow();


    public FrameApp2() {
        super("Алгоритм Дейкстри");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton startAlg = new JButton("Start algorithm");
        Container contentPane = getContentPane();
        JPanel graphContent = new JPanel(new FlowLayout(FlowLayout.CENTER));
        List<DrawableGraph> drawableGraphs = xmlParser.parseGraphs(PATH);
        drawableGraphs.forEach(graphContent::add);
        contentPane.add(startAlg, BorderLayout.SOUTH);
        contentPane.add(graphContent, BorderLayout.CENTER);
        startAlg.addActionListener(e -> {
            for (DrawableGraph drawableGraph : drawableGraphs) {
                findWay(drawableGraph);
            }
            repaint();
        });
        setSize(1000, 1000);
        setVisible(true);
    }

    public void findWay(DrawableGraph graph) {
        Thread thread = new Thread(() ->
        {
            flowSearch.edmondsKarp(graph);
//            kraskala.findMinimalTree(graph);
//            dijkstra2.calculateShortestPathFromSourceAndPaint(graph, graph.getDrawableDots()
//                    .stream()
//                    .filter(drawableDot -> drawableDot.getId() == 3)
//                    .findFirst()
//                    .get());
        });
        thread.start();
    }

    public static void main(String[] args) {
        new FrameApp2();
    }
}
