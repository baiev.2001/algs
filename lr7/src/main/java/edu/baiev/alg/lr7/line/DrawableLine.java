package edu.baiev.alg.lr7.line;

import edu.baiev.alg.lr7.DrawableDot;
import lombok.*;

import javax.swing.*;
import java.awt.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString()
@EqualsAndHashCode(of = "id", callSuper = false)
public class DrawableLine extends JComponent {
    private int id;
    private int weight;
    private int flow = 0;
    private int capacity;
    private int dotIdFrom;
    private int dotIdTo;

    private DrawableDot drawableDotFrom;
    private DrawableDot drawableDotTo;

    private Color color = Color.BLACK;

    public DrawableLine(int id, int weight, int dotIdFrom, int dotIdTo, Color color) {
        this.id = id;
        this.weight = weight;
        this.dotIdFrom = dotIdFrom;
        this.dotIdTo = dotIdTo;
        this.color = color;
    }

    public DrawableLine(int id, int weight, int dotIdFrom, int dotIdTo, int capacity) {
        this.id = id;
        this.weight = weight;
        this.capacity = capacity;
        this.dotIdFrom = dotIdFrom;
        this.dotIdTo = dotIdTo;
        this.color = Color.BLACK;
    }

    public void setColor(Color color) {
        this.color = color;
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(color);
        g.drawLine(this.drawableDotFrom.getX(), this.drawableDotFrom.getY(), this.drawableDotTo.getX(), this.drawableDotTo.getY());
        int xMiddle = (drawableDotFrom.getX() + drawableDotTo.getX()) / 2;
        int yMiddle = (drawableDotFrom.getY() + drawableDotTo.getY()) / 2;
        g.setColor(Color.RED);
//        g.drawString(Integer.toString(this.weight), xMiddle, yMiddle + 4);
        g.drawString(String.format("%d/%d", this.capacity, this.flow), xMiddle, yMiddle + 4);
    }

    public void increaseFlow(int addition) {
        this.flow += addition;
    }
}
