package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.line.DrawableLine;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class DrawableXMLParser {

    public List<DrawableGraph> parseGraphs(String filename) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            File file = new File(filename);
            ArrayList<DrawableGraph> resultList = new ArrayList<>();
            Document doc = documentBuilder.parse(file);
            NodeList childNodes = doc.getElementsByTagName("graph");
            int l = childNodes.getLength();
            for (int i = 0; i < l; ++i) {
                Node graphNode = childNodes.item(i);
                NamedNodeMap itemAttributes = graphNode.getAttributes();
                NodeList graphChilds = graphNode.getChildNodes();
                Set<DrawableDot> drawableDots = parseDots(graphChilds.item(3));
                Set<DrawableLine> drawableLines = parseLines(graphChilds.item(5))
                        .stream()
                        .map(drawableLine -> {
                            DrawableDot drawableDotTo = drawableDots.stream()
                                    .filter(dot -> dot.getId() == drawableLine.getDotIdTo())
                                    .findFirst()
                                    .orElseThrow(() -> new RuntimeException("Can't find dot to " + drawableLine.getId()));
                            drawableLine.setDrawableDotTo(drawableDotTo);
                            DrawableDot drawableDotFrom = drawableDots.stream()
                                    .filter(dot -> dot.getId() == drawableLine.getDotIdFrom())
                                    .findFirst()
                                    .orElseThrow(() -> new RuntimeException("Can't find dot from " + drawableLine.getId()));
                            drawableLine.setDrawableDotFrom(drawableDotFrom);
                            return drawableLine;
                        })
                        .collect(Collectors.toSet());
                resultList.add(new DrawableGraph(
                        Integer.parseInt(itemAttributes.getNamedItem("id").getNodeValue()),
                        drawableDots,
                        drawableLines
                ));
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        throw new RuntimeException();
    }

    private Set<DrawableLine> parseLines(Node lines) {
        if (!lines.getNodeName().equals("lines")) {
            throw new RuntimeException("it is not lines. " + lines.getNodeName());
        }
        Set<DrawableLine> resultSet = new HashSet<>();
        NodeList linesList = lines.getChildNodes();
        int l = linesList.getLength();
        for (int i = 0; i < l; ++i) {
            Node item = linesList.item(i);
            if (item.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            NamedNodeMap itemAttributes = item.getAttributes();
            resultSet.add(new DrawableLine(
                    Integer.parseInt(itemAttributes.getNamedItem("id").getNodeValue()),
                    Integer.parseInt(itemAttributes.getNamedItem("weight").getNodeValue()),
                    Integer.parseInt(itemAttributes.getNamedItem("from").getNodeValue()),
                    Integer.parseInt(itemAttributes.getNamedItem("to").getNodeValue()),
                    Integer.parseInt(itemAttributes.getNamedItem("capacity").getNodeValue())));
        }
        return resultSet;
    }

    private Set<DrawableDot> parseDots(Node points) {
        if (!points.getNodeName().equals("points")) {
            throw new RuntimeException("it is not points. But: " + points.getNodeName());
        }
        Set<DrawableDot> resultSet = new HashSet<>();
        NodeList pointList = points.getChildNodes();
        int l = pointList.getLength();
        for (int i = 0; i < l; ++i) {
            Node point = pointList.item(i);
            if (point.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            NamedNodeMap itemAttributes = point.getAttributes();
            resultSet.add(new DrawableDot(
                    Integer.parseInt(itemAttributes.getNamedItem("id").getNodeValue()),
                    Integer.parseInt(itemAttributes.getNamedItem("x").getNodeValue()),
                    Integer.parseInt(itemAttributes.getNamedItem("y").getNodeValue()),
                    Color.black));
        }
        return resultSet;
    }

//    Code below (two methods) is never used.
//    I leave it to not forgot my realization

    public List<DrawableDot> parseDots(String filename) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            File file = new File(filename);
            ArrayList<DrawableDot> resultList = new ArrayList<DrawableDot>();
            Document doc = documentBuilder.parse(file);
//            System.out.println(doc.getElementsByTagName("point"));
            NodeList childNodes = doc.getElementsByTagName("point");
            int l = childNodes.getLength();
            for (int i = 0; i < l; ++i) {
                Node item = childNodes.item(i);
                NamedNodeMap itemAttributes = item.getAttributes();
                resultList.add(new DrawableDot(
                        Integer.parseInt(itemAttributes.getNamedItem("id").getNodeValue()),
                        Integer.parseInt(itemAttributes.getNamedItem("x").getNodeValue()),
                        Integer.parseInt(itemAttributes.getNamedItem("y").getNodeValue()),
                        Color.black));
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        throw new RuntimeException();
    }

    public List<DrawableLine> parseLines(String filename) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(true);
            factory.setIgnoringElementContentWhitespace(true);
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            File file = new File(filename);
            ArrayList<DrawableLine> resultList = new ArrayList<>();
            Document doc = documentBuilder.parse(file);
            NodeList childNodes = doc.getElementsByTagName("line");
            int l = childNodes.getLength();
            for (int i = 0; i < l; ++i) {
                Node item = childNodes.item(i);
                NamedNodeMap itemAttributes = item.getAttributes();
                resultList.add(new DrawableLine(
                        Integer.parseInt(itemAttributes.getNamedItem("id").getNodeValue()),
                        Integer.parseInt(itemAttributes.getNamedItem("weight").getNodeValue()),
                        Integer.parseInt(itemAttributes.getNamedItem("from").getNodeValue()),
                        Integer.parseInt(itemAttributes.getNamedItem("to").getNodeValue()),
                        Color.black));
            }
            return resultList;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        throw new RuntimeException();
    }
}
