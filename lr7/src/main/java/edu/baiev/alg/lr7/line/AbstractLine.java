package edu.baiev.alg.lr7.line;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractLine {

    protected int id;

    protected int weight;

    protected int dotIdFrom;

    protected int dotIdTo;
}
