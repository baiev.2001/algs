package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.line.DrawableLine;
import edu.baiev.alg.lr7.line.LineService;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

public class DrawableFlow {
    private LineService lineService = new LineService();
    private static final List<Color> availableVectorColors = Arrays.asList(
            Color.ORANGE,
            Color.RED,
            Color.GREEN,
            Color.CYAN,
            Color.MAGENTA,
            new Color(104, 25, 215),
            new Color(177, 208, 19, 255)
    );

    public int edmondsKarp(DrawableGraph graph) {
        Set<DrawableLine> drawableLines = graph.getDrawableLines();
        // Существующий поток удаляется.
        clearFlow(drawableLines);
        int size = graph.getDrawableDots().size();
        // Цикл по увеличению потока в сети, пока имеется путь
        // с положительной нагрузкой от истока к стоку в остаточной сети.
        int foundPath = 100;
        do {
            foundPath = searchPath(drawableLines, size);
            graph.repaint();
            waitForAnimation();
        } while (foundPath > 0);
        // Величина максимального потока - сумма величин потока на дугах,
        // исходящих из истока.
        int maxFlow = 0;
        for (DrawableLine line : drawableLines) {
            maxFlow += line.getFlow();
        }
        return maxFlow;
    }

    private void clearFlow(Set<DrawableLine> drawableLine) {
        drawableLine.forEach(line -> line.setFlow(0));
    }


    private int searchPath(Set<DrawableLine> sourceLines, int numberOfNods) {
        DrawableLine[] path = new DrawableLine[sourceLines.size()];
        // Отметка пройденных вершин. Вершина считается пройденной в тот момент,
        // Когда ее впервые захватывает "волна".
        boolean[] passed = new boolean[numberOfNods];
        // Очередь вершин для обхода.
        LinkedList<DrawableDot> queue = new LinkedList<>();
        DrawableDot source = getSource(sourceLines);
        DrawableDot sink = getSink(sourceLines);
        // Обход по остаточной сети.
        queue.addLast(source);
        passed[source.getId() - 1] = true;
        while (!queue.isEmpty()) {
            DrawableDot u = queue.removeFirst();
            for (DrawableLine line : lineService.getLinesByDot(u, sourceLines)) {
                DrawableDot end = null;
                if (line.getDrawableDotFrom().equals(u) && !passed[line.getDotIdTo() - 1] && line.getFlow() < line.getCapacity()) {
                    // Прямая дуга, можно увеличить поток вдоль дуги.
                    end = line.getDrawableDotTo();
                } else if (line.getDrawableDotTo().equals(u) && !passed[line.getDotIdFrom() - 1] && line.getFlow() > 0) {
                    // Обратная дуга, можно уменьшить поток вдоль дуги.
                    end = line.getDrawableDotFrom();
                }
                if (Objects.nonNull(end)) {
                    // Найдена новая вершина. Пополняем дерево обхода.
                    path[end.getId()] = line;
                    if (end.equals(sink)) {
                        // Найден путь в остаточной сети!
                        return modifyPath(path, sink, source);
                    } else {
                        passed[end.getId() - 1] = true;
                        queue.add(end);
                    }
                }
            }
        }
        // Путь не найден.
        return 0;
    }

    private int modifyPath(DrawableLine[] path, DrawableDot sink, DrawableDot source) {
        int addition = Integer.MAX_VALUE;
        DrawableDot pred;
        DrawableDot next = sink;
        while (!next.equals(source)) {
            DrawableLine line = path[next.getId()];
            line.setColor(Color.RED);
            if (line.getDrawableDotTo().equals(next)) {
                pred = line.getDrawableDotFrom();
                if (addition > line.getCapacity() - line.getFlow()) {
                    addition = line.getCapacity() - line.getFlow();
                }
            } else {
                pred = line.getDrawableDotTo();
                if (addition > line.getFlow()) {
                    addition = line.getFlow();
                }
            }
            next = pred;
        }
        // Производим изменение потока вдоль пути.
        next = sink;
        while (next != source) {
            DrawableLine line = path[next.getId()];
            if (line.getDrawableDotTo().equals(next)) {
                pred = line.getDrawableDotFrom();
                line.increaseFlow(addition);
            } else {
                pred = line.getDrawableDotTo();
                line.increaseFlow(-addition);
            }
            next = pred;
        }
        return addition;
    }

    private DrawableDot getSink(Set<DrawableLine> sourceLines) {
        return sourceLines.stream()
                .flatMap(line -> Stream.of(line.getDrawableDotTo(), line.getDrawableDotFrom()))
                .distinct()
                .filter(dot -> dot.getId() == 9)
                .findFirst()
                .get();
    }

    private DrawableDot getSource(Set<DrawableLine> sourceLines) {
        return sourceLines.stream()
                .flatMap(line -> Stream.of(line.getDrawableDotTo(), line.getDrawableDotFrom()))
                .distinct()
                .filter(dot -> dot.getId() == 3)
                .findFirst()
                .get();
    }

    private void waitForAnimation() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
