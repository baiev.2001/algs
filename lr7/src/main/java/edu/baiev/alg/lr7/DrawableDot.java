package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.line.DrawableLine;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedList;
import java.util.List;

import static edu.baiev.alg.lr7.Constants.MULTIPLAYER;

@NoArgsConstructor
@Setter
@Getter
@ToString(exclude = "shortestPath")
@EqualsAndHashCode(of = "id", callSuper = false)
public class DrawableDot extends JComponent {
    private int id;

    private int x;
    private int y;
    private Color color;

    private List<DrawableLine> shortestPath = new LinkedList<>();
    private int distance = Integer.MAX_VALUE;

    public DrawableDot(int id, int x, int y) {
        this.id = id;
        this.x = x * MULTIPLAYER;
        this.y = y * MULTIPLAYER;
        this.color = Color.BLACK;
    }

    public DrawableDot(int id, int x, int y, Color color) {
        this.id = id;
        this.x = x * MULTIPLAYER;
        this.y = y * MULTIPLAYER;
        this.color = color;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.black);
        g.setColor(this.color);
        g.fillOval(x, y, 10, 10);
        g.drawString(Integer.toString(this.id), x - 1, y - 1);
        if (distance != Integer.MAX_VALUE) {
            g.drawString("Distance: " + this.distance, x - 20, y + 20);
        }
    }
}
