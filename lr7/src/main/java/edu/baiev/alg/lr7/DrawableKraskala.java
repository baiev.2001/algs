package edu.baiev.alg.lr7;

import edu.baiev.alg.lr7.kraskala.DisjointSetUnion;
import edu.baiev.alg.lr7.kraskala.DisjointSetUnionIntImpl;
import edu.baiev.alg.lr7.line.DrawableLine;

import java.awt.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DrawableKraskala {

    public void findMinimalTree(DrawableGraph graph) {
        DisjointSetUnion<Integer> disjointUnion = new DisjointSetUnionIntImpl(graph.getDrawableDots().size() + 1);
        List<DrawableLine> orderedLines = graph.getDrawableLines().stream()
                .sorted(Comparator.comparingInt(DrawableLine::getWeight))
                .collect(Collectors.toList());
        graph.getDrawableDots()
                .forEach(dot -> disjointUnion.makeSet(dot.getId()));
        for (DrawableLine line : orderedLines) {
            if (disjointUnion.unite(line.getDrawableDotTo().getId(), line.getDrawableDotFrom().getId())) {
                line.setColor(Color.RED);
                graph.repaint();
                waitForAnimation();
            }
        }
    }

    private void waitForAnimation() {
        try {
            Thread.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
