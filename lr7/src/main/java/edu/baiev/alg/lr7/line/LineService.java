package edu.baiev.alg.lr7.line;

import edu.baiev.alg.lr7.DrawableDot;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class LineService {
    public List<DrawableLine> getLinesByStartDot(DrawableDot node, Collection<DrawableLine> lines) {
        return lines.stream()
                .filter(line -> line.getDrawableDotFrom().getId() == node.getId())
                .collect(Collectors.toList());
    }

    public List<DrawableLine> getLinesByDot(DrawableDot node, Collection<DrawableLine> lines) {
        return lines.stream()
                .filter(line -> line.getDrawableDotFrom().getId() == node.getId() || line.getDrawableDotTo().getId() == node.getId())
                .collect(Collectors.toList());
    }

    public DrawableLine getLightestEdgeByDot(DrawableDot dot, Collection<DrawableLine> lines) {
        return lines.stream()
                .filter(line -> line.getDrawableDotFrom().equals(dot) || line.getDrawableDotTo().equals(dot))
                .min(Comparator.comparingInt(DrawableLine::getWeight))
                .orElseThrow(() -> new RuntimeException("There is no line for dot a" + dot));
    }

    public DrawableDot getOppositeDot(DrawableLine line, DrawableDot dot) {
        if (line.getDrawableDotTo().equals(dot)) {
            return line.getDrawableDotFrom();
        } else if (line.getDrawableDotFrom().equals(dot)) {
            return line.getDrawableDotTo();
        } else {
            throw new RuntimeException("This line " + line + " is not linked to dot! " + dot.getId());
        }
    }
}
