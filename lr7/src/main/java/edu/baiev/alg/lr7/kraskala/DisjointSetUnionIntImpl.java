package edu.baiev.alg.lr7.kraskala;

public class DisjointSetUnionIntImpl implements DisjointSetUnion<Integer> {
    private Integer[] array;
    private int[] rank;

    public DisjointSetUnionIntImpl(int size) {
        this.array = new Integer[size];
        this.rank = new int[size];
    }

    @Override
    public void makeSet(Integer item) {
        array[item] = item;
    }

    @Override
    public int find(Integer item) {
        if (array[item].equals(item)) {
            return item;
        }
        return array[item] = find(array[item]);
    }

    @Override
    public boolean unite(Integer item1, Integer item2) {
        item1 = find(item1);
        item2 = find(item2);
        if (item1.equals(item2)) {
            return false;
        }
        if (rank[item1] < rank[item2]) {
            array[item1] = item2;
        } else {
            array[item2] = item1;
            if (rank[item1] == rank[item2])
                ++rank[item1];
        }
        return true;
    }
}
