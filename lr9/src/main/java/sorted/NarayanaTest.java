package sorted;

import java.util.Arrays;
import java.util.Scanner;

import static java.util.Arrays.*;

public class NarayanaTest {

    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        Narayana<Long> narayana = new Narayana<>();
        Long[] array = {28524L, 63386L, 91651L, 133873L, 150233L, 242235L, 357027L, 387763L, 396733L, 437535L};

//        System.out.println("Неубывающая последовательность и её перестановки:");
        do {
            System.out.println(Arrays.deepToString(array));
        } while (narayana.nextPermutation(array));
        // x < y — критерий сравнения для неубывающей последовательности
//        System.out.println("Невозрастающая последовательность и её перестановки:");
//        do {
//            System.out.println(Arrays.deepToString(sequence));
//        } while (Narayana.nextPermutation(sequence, NarayanaTest::greater));
        // x > y — критерий сравнения для невозрастающей последовательности
        System.out.println(System.currentTimeMillis() - l);
        System.out.println(Narayana.swaps);
    }
}
