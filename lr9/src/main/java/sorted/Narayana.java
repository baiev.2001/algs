package sorted;

public class Narayana<Z extends Comparable<Z>> {
    public static Integer swaps = 0;

    // Поиск очередной перестановки
    public boolean nextPermutation(Z[] sequence) {
        int i = sequence.length;
        do {
            if (i < 2) {
                return false; // Перебор закончен
            }
            --i;
        } while (this.greater(sequence[i - 1], sequence[i]));
        int j = sequence.length;
        while (i < j && !this.greater(sequence[i - 1], sequence[--j])) ;
        swapItems(sequence, i - 1, j);
        ++swaps;
        j = sequence.length;
        while (i < --j) {
            swapItems(sequence, i++, j);
            ++swaps;
        }
        return true;
    }

    // Обмен значениями двух элементов последовательности
    private void swapItems(Z[] sequence, int a, int b) {
        Z temp = sequence[a];
        sequence[a] = sequence[b];
        sequence[b] = temp;
    }

    // Возвращает true, если a меньше b, иначе — false
    private boolean less(final Z a, final Z b) {
        return a.compareTo(b) < 0;
    }

    // Возвращает true, если a больше b, иначе — false
    private boolean greater(Z a, final Z b) {
        return a.compareTo(b) > 0;
    }
}
