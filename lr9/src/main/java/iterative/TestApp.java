package iterative;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

public class TestApp {

    public static void main(String[] args) {
        long[] sourceArray = {91651, 28524, 150233, 357027, 387763, 63386, 396733, 133873, 437535, 462783};
        long l = System.currentTimeMillis();
        PermutationsIterativeImpl<Long> permutationsIterative = new PermutationsIterativeImpl<>();
        permutationsIterative.generate(sourceArray.length, Arrays.stream(sourceArray).boxed().toArray(Long[]::new));
        System.out.println(System.currentTimeMillis() - l);
    }
}
