package iterative;

import java.util.Arrays;

public class PermutationsIterativeImpl<T> {
    public void generate(int n, T[] array) {
        int swaps = 0;
        int[] indexes = new int[n];
        for (int i = 0; i < n; i++) {
            indexes[i] = 0;
        }
        int i = 0;
        while (i < n) {
            if (indexes[i] < i) {
                swap(array, i % 2 == 0 ? 0 : indexes[i], i);
                ++swaps;
                indexes[i]++;
                i = 0;
            } else {
                indexes[i] = 0;
                i++;
            }
        }
        System.out.println(swaps);
    }

    private void swap(T[] input, int a, int b) {
        T tmp = input[a];
        input[a] = input[b];
        input[b] = tmp;
    }

    private void saveState(T[] input) {
        System.out.println(Arrays.deepToString(input));
    }
}
