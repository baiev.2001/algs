package recursive;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PermutationsRecursiveImpl<T> {
    private final Collection<T[]> resultArray = new ArrayList<>();

    public Collection<T[]> generate(int n, T[] array) {
        if (n == 1) {
            saveState(array, resultArray);
        } else {
            for (int i = 0; i < n - 1; i++) {
                generate(n - 1, array);
                if (n % 2 == 0) {
                    swap(array, i, n - 1);
                } else {
                    swap(array, 0, n - 1);
                }
            }
            generate(n - 1, array);
        }
        return resultArray;
    }

    private void swap(T[] input, int a, int b) {
        T tmp = input[a];
        input[a] = input[b];
        input[b] = tmp;
    }

    private void saveState(T[] input, Collection<T[]> output) {
        output.add(input);
    }
}
