<h2>Algorithms project</h2>
Project for algorithms subject

Done by Baiev Viktor; Student of 305 group

* lr4 - contains labs from 4 to 6. Sort algorithms. 
* lr7 - contains lab 7 - 9.
    *  Dijkstra graph algorithm.
    *  Kraskala and Prima graph algorithm
    *  Search of maximum available flow algorithm
    
lr 10 - contains combinatorics algorithms